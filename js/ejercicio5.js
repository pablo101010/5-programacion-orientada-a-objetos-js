// Generaciones
// 5- Crea una clase llamada Persona que siga las siguientes condiciones:
// Sus propiedades son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura, año de nacimiento. Si quieres añadir alguna propiedad extra puedes hacerlo.
// Los métodos que se debe poder utilizar  son:
// mostrarGeneracion: este método debe mostrar un mensaje indicando a qué generación pertenece la persona creada y cual es el rasgo característico de esta generación.
// Para realizar este método tener en cuenta la siguiente tabla de generaciones:

class Persona {
    constructor(nombre, edad, Dni, sexo, peso, altura, AdNac) {
        this.nombre = nombre;
        this.edad = edad;
        this.Dni = Dni;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
        this.AdNac = AdNac;
    }

    //metodos
    mostrarGeneracion() {
        if (this.AdNac > 1929 && this.AdNac < 1949)
            document.write(`<br>  La persona ${this.nombre} pertenece a la generacion "Silent Generation"`)

        if (this.AdNac > 1948 && this.AdNac < 1969)
            document.write(`<br> La persona ${this.nombre} pertenece a la generacion "Baby Boom"`)

        if (this.AdNac > 1968 && this.AdNac < 1981)
            document.write(`<br> La persona ${this.nombre} pertenece a la generacion "Generacion X"`)

        if (this.AdNac > 1980 && this.AdNac < 1994)
            document.write(`<br> La persona ${this.nombre} pertenece a la generacion "Generacion Y"`)

        if (this.AdNac > 1993 && this.AdNac < 2011)
            document.write(`<br> La persona ${this.nombre} pertenece a la generacion "Generacion Z"`)
    }

}

//Crear objeto tipo "PERSONA"
let Eduardo = new Persona("Eduardo", 48, 35305989, "H", 80, 1.75, 1935)
//console.log(Eduardo.AdNac)
Eduardo.mostrarGeneracion();


let Patricia = new Persona("Patricia", 32, 35302356, "M", 80, 1.75, 1988)
Patricia.mostrarGeneracion();