//1 - Crea un objeto llamado auto que tenga algunas características como el color, marca, modelo y si está encendido o apagado.Crea los métodos necesarios para permitir encender y apagar el auto.

class auto {
    constructor(colorP,marcaP,modeloP,encendidoP,apagadoP){
    this.color=colorP;
    this.marca=marcaP;
    this.modelo=modeloP;
    this.encendido=encendidoP;
    this.apagado=apagadoP;
}

//metodos
mostrarInfo(){
    document.write(`<br>Auto: ${this.color} - ${this.marca} - ${this.modelo} - ${this.encendido} - ${this.apagado}`)
}

modificarEncendido(){
    this.encendido=false;
    this.apagado=true;
}

}

//crear objeto auto y mostrarlo
let auto1 = new auto("Rojo","Audi",2018,true,false)
auto1.mostrarInfo();

//modifico el encendido
auto1.modificarEncendido();
auto1.mostrarInfo();