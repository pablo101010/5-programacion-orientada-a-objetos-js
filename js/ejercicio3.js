//3-Escribe una clase que permita crear distintos objetos “rectángulos”, con las propiedades de alto y ancho, mas los métodos necesarios para modificar y mostrar sus propiedades, calcular el perímetro y el área

//no funciona lo del area y del perimetro ---->>>> mostrarPropiedades()

class Rectangulo {
    constructor(base, altura, area, perimetro) {
        this.base = base;
        this.altura = altura;
        this.area = area;
        this.perimetro = perimetro;
    }

    mostrarDatos() {
        document.write(`<br>La base del Rectangulo es de: ${this.base} <br>
    La altura del rectangulo es de ${this.altura} `);
    }

    modificarDatos() {
        this.base = parseInt(prompt("Ingrese la base"));
        this.altura = parseInt(prompt("Ingrese la altura"));
    }

    /*set modificarArea(){ //MODIFICAR el valor de una sola propiedad
        this.area=base * altura;;
    }*/

    mostrarPropiedades() {
        this.area = this.base * this.altura;
        this.perimetro = (this.base * 2) + (this.altura * 2);
        document.write(`<br>El area del Rectangulo es de: ${this.area} <br>
    El perimetro del rectangulo es de ${this.perimetro} `);
    }
}

let rectangulo1 = new Rectangulo(5, 10, 0, 0);
rectangulo1.mostrarDatos();

rectangulo1.modificarDatos();
rectangulo1.mostrarDatos();

rectangulo1.mostrarPropiedades();

