/*2-Escribe un programa que cree un objeto "cuenta" con las siguientes propiedades:
Una propiedad titular con el valor "Alex".
Una propiedad saldo, teniendo como valor inicial 0.
Un método ingresar() que permita añadir dinero a la cuenta, pasando la cantidad como parámetro
Un método extraer() que permita retirar la cantidad pasada como parámetro.
Un método informar() que retorne la información del estado de la cuenta. 
Utiliza este objeto para mostrar la descripción, ingresar y extraer dinero y volver a mostrar la descripción del estado de la cuenta.
*/

class cuenta {
    constructor(titularP, saldoP, ingresarP, extraerP) {
        this.titular = titularP;
        this.saldo = saldoP;
        this.ingresar = ingresarP;
        this.extraer = extraerP;
    }

    //metodos

    Ingresar(montoIngresado) {
        this.saldo = this.saldo + montoIngresado;
    }

    Extraer(montoExtraido) {
        this.saldo = this.saldo - montoExtraido;
    }

    Informar() {
        document.write(`<br>Cuenta: ${this.titular} - ${this.saldo} - ${this.ingresar} - ${this.extraer} - ${this.informar}`)
    }

}


let cuenta1 = new cuenta("Alex", 0, 0, 0)
document.write("<br> Estado de cuenta Inicial: ");
cuenta1.Informar();

document.write("<br> Estado de cuenta despues de ingresar: ");
cuenta1.Ingresar(1200);
cuenta1.Informar();

document.write("<br> Estado de cuenta despues de extraer ");
cuenta1.Extraer(100);
cuenta1.Informar();

