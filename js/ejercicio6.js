// Libros
// Crear una clase Libro que contenga al menos las siguientes propiedades:
// ISBN
// Título
// Autor
// Número de páginas

// Crear sus respectivos métodos get y set correspondientes para cada propiedad. Crear el método mostrarLibro() para mostrar la información relativa al libro con el siguiente formato:

// “El libro xxx con ISBN xxx creado por el autor xxx tiene páginas xxx”

// Crear al menos 2 objetos libros y utilizar el método mostrarLibro();
// Por último, indicar cuál de los 2 objetos “libros” tiene más páginas.

class Libro {
    constructor(isbn, titulo, autor, nroDePag) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.autor = autor;
        this.nroDePag = nroDePag;
    }

    get mostrarNombre() {
        return this.titulo;
    }

    set modificarNombre(nombreNuevo) {
        this.titulo = nombreNuevo;
    }

    mostrarLibro() {
        document.write(`<br> El libro ${this.titulo} con ISBN ${this.isbn} creado por el autor ${this.autor} tiene páginas ${this.nroDePag}`)
    }
}

let principito=new Libro(4215, "El principito", "Phill Col", 682);
let rayuela=new Libro(1538, "Rayuela", "Cortazar", 1521);

principito.mostrarLibro();
rayuela.mostrarLibro();

if (principito.nroDePag > rayuela.nroDePag)
document.write("El libro principito tiene un mayor numero de paginas")
else
document.write("El libro Rayuela tiene un mayor numero de paginas")
