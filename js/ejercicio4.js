/*4- Escribe una clase Producto para crear objetos. Estos objetos, deben presentar las propiedades código, nombre y precio, además del método imprime datos, el cual escribe por pantalla los valores de sus propiedades.
Posteriormente, cree tres instancias de este objeto y guárdalas en un array.
Por último, utilice el método imprime datos para mostrar por pantalla los valores de los tres objetos instanciados.
*/

class producto{
    constructor(codigo, nombre, precio){
        this.codigo=codigo;
        this.nombre=nombre;
        this.precio=precio;
    }

imprimeDatos(){
    document.write(`El producto ${this.nombre} con el codigo ${this.codigo}, tiene un precio de ${this.precio}`)
}

}

let prod=[3];
prod[1]=new producto(10253,"Arroz",50);
prod[2]=new producto(23651,"Fideos",80);
prod[3]=new producto(89475,"Jabon", 40);


for(let i=1; i<4;i++){
    prod[i].imprimeDatos();
    document.write("<br>");
}
